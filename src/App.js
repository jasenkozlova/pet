import React from 'react';
import { Route } from 'react-router-dom';

import Navigation from './components/Nav/Navigation';
import ProductsPage from './containers/ProductsList/ProductList';
import FavoritesPage from './containers/Favorites';
import  LogInForm from './containers/LogInForm/LogInForm'

const App = props => {
  return (
    <React.Fragment>
      <Navigation />
      <main>
        <Route path="/" component={ProductsPage} exact />
        <Route path="/favorites" component={FavoritesPage} />
        <Route path="/login" component={LogInForm} />
      </main>
    </React.Fragment>
  );
};

export default App;
