import React from 'react';
import { useSelector } from 'react-redux';

import FavoriteItem from '../components/FavoriteItem/FavoriteItem';

const Favorites = props => {
  const favoriteProducts = useSelector(state =>
    state.shop.products.filter(p => p.isFavorite)
  );
  let content = <p className="placeholder">Got no favorites yet!</p>;
  if (favoriteProducts.length > 0) {
    content = (
      <div>
        {favoriteProducts.map(prod => (
          <FavoriteItem
            key={prod.id}
            id={prod.id}
            title={prod.title}
            image={prod.image}
            description={prod.desc}
          />
        ))}
      </div>
    );
  }
  return content;
};

export default Favorites;
