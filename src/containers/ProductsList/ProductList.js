import React from 'react';
import { useSelector } from 'react-redux';

import ProductItem from '../../components/Product/ProductItem';
import classes from './ProductList.css'

const Products = props => {
  const productList = useSelector(state => state.shop.products);
  return (
    <div className={classes.ProductList}>
      {productList.map(prod => (
        <ProductItem
          key={prod.id}
          id={prod.id}
          title={prod.title}
          description={prod.desc}
          image={prod.photo}
          price={prod.price}
          weight={prod.weight}
          isFav={prod.isFavorite}
        />
      ))}
    </div>
  );
};

export default Products;
