import React from 'react';
import InputField from './../../components/UI/InputField/InputField';
import logo from './../../logo.png';
import classes from './LogInForm.css'

const LogInForm = ({ initialData, onSubmit }) => {
    //const [logInData, setLogInData] = initialData;
    const logInData = {
        nickname: 'Vasya',
        email: 'pupkin@gmail.com',
        password: 'Reac5$$$',
      };
    return (
        <div className={classes.LogInForm}>
            <img className={classes.Logo} src={logo}/>
            <form>
                <InputField 
                    label='Enter your nickname'
                    type='text'
                    placeholder= {logInData.nickname}
                    />
                <InputField 
                    label='Enter your e-mail'
                    type='email'
                    placeholder= {logInData.email}
                    />
                <InputField 
                    label='Enter your password'
                    type='password'
                    placeholder= {logInData.password}
                    />
                <button className={classes.Button}>Submit</button>
            </form>
        </div>
    );
  };

  export default LogInForm;