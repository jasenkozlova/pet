import { TOGGLE_FAV } from '../actions/products';

const initialState = {
  products: [
     {
        "categoria" : {
          "gluten-free" : true,
          "vegiterian" : true
        },
        "desc" : "Вегетарианский!! ",
        "photo" : "https://eco-food.com.ua/source/files/new2019/lanch_kur-krilya.jpg",
        "price" : 5.35,
        "title" : "lanch 01",
        "weight" : 400,
        "id": 1,
        "isFavorite": false
      },
      {
        "desc" : "Филе куриное су-вид, кабачок, баклажан, фасоль стручковая, картофель, помидор, перец болгарский, масло растительное",
        "photo" : "https://eco-food.com.ua/source/files/new2019/biz-lanch-ragu.jpg",
        "price" : 3.5,
        "title" : "Lanch 02",
        "weight" : 500,
        "id": 2,
        "isFavorite": false
      },
     {
        "desc" : "Картофельное пюре, телятина, шампиньоны, лук репчатый, сливки, салат овощной",
        "photo" : "https://eco-food.com.ua/source/files/new2019/biz-lanch_befstrog.jpg",
        "price" : 2.5,
        "title" : "Lanch 03",
        "weight" : 450,
        "id": 3,
        "isFavorite": false
      }
  ]
};

const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_FAV:
      const prodIndex = state.products.findIndex(
        p => p.id === action.productId
      );
      const newFavStatus = !state.products[prodIndex].isFavorite;
      const updatedProducts = [...state.products];
      updatedProducts[prodIndex] = {
        ...state.products[prodIndex],
        isFavorite: newFavStatus
      };
      return {
        ...state,
        products: updatedProducts
      };
    default:
      return state;
  }
};

export default productReducer;
