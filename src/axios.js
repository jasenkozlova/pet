import axios from 'axios';

const instance = axios.create ({
        baseURL: 'https://prod-yana.firebaseio.com/'
});

export default instance;