import React from 'react';
import { useDispatch } from 'react-redux';

import Card from '../UI/ProductCard/Card';
import classes from './ProductItem.css';
import { toggleFav } from '../../store/actions/products';

const ProductItem = props => {
  const dispatch = useDispatch();

  const toggleFavHandler = () => {
    dispatch(toggleFav(props.id));
  };

  return (
    <Card style={{ marginBottom: '1rem' }}>
      <div className={classes.ProductItem}>
        <img className={classes.ProductItemImage} src={props.image}/>
        <h2>{props.title}</h2>
        <p>{props.description}</p>
        <div className={classes.flex}>
          <div
            className={!props.isFav ? classes.ButtonUnFav : classes.ButtonFav}
            onClick={toggleFavHandler}>
          </div>
          <div className={classes.ProductPrice}>{props.price} USD</div>
        </div>
      </div>
    </Card>
  );
};

export default ProductItem;
