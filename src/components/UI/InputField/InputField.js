import React from 'react';
import classes from './InputField.css';

const InputField = (props) => {
    return (
    <label className={classes.Input}>
        {props.label}
        <input
          className={classes.InputField}
          type={props.type}
          placeholder={props.placeholder}
          /*onChange={e => setLogInData({ ...logInData, email: e.target.value })}*/
        />
      </label>
    )
}

export default InputField;