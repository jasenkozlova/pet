import React from 'react';
import NavigationItem from './NavigationItem'
import classes from './Navigation.css';

const Navigation = props => {
  return (
    <header className={classes.MainHeader}>
      <nav>
        <NavigationItem link="/" exact>All product</NavigationItem>
        <NavigationItem link="/favorites">Favorites</NavigationItem>
        <NavigationItem link="/login">Log in</NavigationItem>
      </nav>
    </header>
  );
};

export default Navigation;
