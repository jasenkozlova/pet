import React from 'react';
import { useDispatch } from 'react-redux';
import { toggleFav } from '../../store/actions/products';
import Card from '../UI/ProductCard/Card';
import classes from './FavoriteItem.css';

const FavoriteItem = props => {
  const dispatch = useDispatch();

  const toggleFavHandler = () => {
    dispatch(toggleFav(props.id));
  };
  return (
    <Card className={classes.FavoriteItem} style={{ marginBottom: '1rem', height: '100px', width: '300px'}}>
      <div className={classes.FavoriteItem}>
        <h2>{props.title}</h2>
        <div
            className={!props.isFav ? classes.ButtonFav : ''}
            onClick={toggleFavHandler}>
          </div>
      </div>
    </Card>
  );
};

export default FavoriteItem;

